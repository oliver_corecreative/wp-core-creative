<?php
/**
 * Created by PhpStorm.
 * User: valleyview
 * Date: 14/07/2014
 * Time: 17:10
 */


/*
 * Get previous post link that is adjacent to the current post.
 *
 * @since 3.7.0
 *
 * @param string       $format         Optional. Link anchor format.
 * @param string       $link           Optional. Link permalink format.
 * @param bool         $in_same_term   Optional. Whether link should be in a same taxonomy term.
 * @param array|string $excluded_terms Optional. Array or comma-separated list of excluded term IDs.
 * @param string       $taxonomy       Optional. Taxonomy, if $in_same_term is true. Default 'category'.
 * @return string
 */
function cc_get_previous_post_link( $title, $atts = '', $format = '&laquo; %link', $link = '%title', $in_same_cat = false, $excluded_terms = '', $taxonomy = 'category' ) {
    return cc_get_adjacent_post_link( $title, $atts, $format, $link, $in_same_cat, $excluded_terms, true, $taxonomy );
}

/**
 * Display previous post link that is adjacent to the current post.
 *
 * @since 1.5.0
 * @see get_previous_post_link()
 *
 * @param string       $format         Optional. Link anchor format.
 * @param string       $link           Optional. Link permalink format.
 * @param bool         $in_same_term   Optional. Whether link should be in a same taxonomy term.
 * @param array|string $excluded_terms Optional. Array or comma-separated list of excluded term IDs.
 * @param string       $taxonomy       Optional. Taxonomy, if $in_same_term is true. Default 'category'.
 */
function cc_previous_post_link( $title, $atts = '', $format = '&laquo; %link', $link = '%title', $in_same_cat = false, $excluded_terms = '', $taxonomy = 'category' ) {
    echo cc_get_previous_post_link( $title, $atts, $format, $link, $in_same_cat, $excluded_terms, $taxonomy );
}

/**
 * Get next post link that is adjacent to the current post.
 *
 * @since 3.7.0
 *
 * @param string       $format         Optional. Link anchor format.
 * @param string       $link           Optional. Link permalink format.
 * @param bool         $in_same_term   Optional. Whether link should be in a same taxonomy term.
 * @param array|string $excluded_terms Optional. Array or comma-separated list of excluded term IDs.
 * @param string       $taxonomy       Optional. Taxonomy, if $in_same_term is true. Default 'category'.
 * @return string
 */
function cc_get_next_post_link( $title, $atts = '', $format = '%link &raquo;', $link = '%title', $in_same_cat = false, $excluded_terms = '', $taxonomy = 'category' ) {
    return cc_get_adjacent_post_link( $title, $atts, $format, $link, $in_same_cat, $excluded_terms, false, $taxonomy );
}

/**
 * Display next post link that is adjacent to the current post.
 *
 * @since 1.5.0
 * @see get_next_post_link()
 *
 * @param string       $format         Optional. Link anchor format.
 * @param string       $link           Optional. Link permalink format.
 * @param bool         $in_same_term   Optional. Whether link should be in a same taxonomy term.
 * @param array|string $excluded_terms Optional. Array or comma-separated list of excluded term IDs.
 * @param string       $taxonomy       Optional. Taxonomy, if $in_same_term is true. Default 'category'.
 */
function cc_next_post_link( $title, $atts = '', $format = '%link &raquo;', $link = '%title', $in_same_cat = false, $excluded_terms = '', $taxonomy = 'category' ) {
    echo cc_get_next_post_link( $title, $atts, $format, $link, $in_same_cat, $excluded_terms, $taxonomy );
}



/**
 * Get adjacent post link.
 *
 * Can be either next post link or previous.
 *
 * @since 3.7.0
 *
 * @param string       $format         Link anchor format.
 * @param string       $link           Link permalink format.
 * @param bool         $in_same_term   Optional. Whether link should be in a same taxonomy term.
 * @param array|string $excluded_terms Optional. Array or comma-separated list of excluded terms IDs.
 * @param bool         $previous       Optional. Whether to display link to previous or next post. Default true.
 * @param string       $taxonomy       Optional. Taxonomy, if $in_same_term is true. Default 'category'.
 * @return string
 */
function cc_get_adjacent_post_link( $title = '', $attr = '', $format, $link, $in_same_cat = false, $excluded_terms = '', $previous = true, $taxonomy = 'category' ) {
    if ( $previous && is_attachment() )
        $post = get_post( get_post()->post_parent );
    else
        $post = get_adjacent_post( $in_same_cat, $excluded_terms, $previous, $taxonomy );



    if ( ! $post ) {
        $output = '';
    } else {

        if(empty($title))
            $title = $previous ? __( 'Previous Post' ) : __( 'Next Post' );

        $date = mysql2date( get_option( 'date_format' ), $post->post_date );
        $rel = $previous ? 'prev' : 'next';

        $default_attr = array(
            'href'	=> get_permalink( $post ),
            'rel'	=> $rel
        );

        $attr = wp_parse_args($attr, $default_attr);

        $attr = array_map( 'esc_attr', $attr );
        $html = rtrim("<a ");
        foreach ( $attr as $name => $value ) {
            $html .= " $name=" . '"' . $value . '"';
        }
        $html .= '>' . $title . '</a>';

        $output = $html;
    }

    $adjacent = $previous ? 'previous' : 'next';

    /**
     * Filter the adjacent post link.
     *
     * The dynamic portion of the hook name, $adjacent, refers to the type
     * of adjacency, 'next' or 'previous'.
     *
     * @since 2.6.0
     *
     * @param string  $output The adjacent post link.
     * @param string  $format Link anchor format.
     * @param string  $link   Link permalink format.
     * @param WP_Post $post   The adjacent post.
     */
    return apply_filters( "{$adjacent}_post_link", $output, $format, $link, $post );
}


function cc_get_recent_posts($args) {
    // Set default arguments
    $defaults = array(
        'numberposts' => 10, 'offset' => 0,
        'category' => 0, 'orderby' => 'post_date',
        'order' => 'DESC', 'include' => '',
        'exclude' => '', 'meta_key' => '',
        'meta_value' =>'', 'post_type' => 'post', 'post_status' => 'draft, publish, future, pending, private',
        'suppress_filters' => true
    );

    $r = wp_parse_args( $args, $defaults );

    return new WP_Query( $r );
}

function cc_is_ajax() {
    return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}
