<?php
/**
 * Created by PhpStorm.
 * User: valleyview
 * Date: 28/03/2014
 * Time: 10:32
 */

add_action( 'login_enqueue_scripts', function(){
    ?>

    <style type="text/css">
        body.login {
            /* Set the size of the login logo */
            /* Hide the "Back to (website)" link */
            /* Hide the "Log in" link on the last password page, as it's pointless*/
            /* Center align the Lost Password link */
            background-color: #ff475f;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -ms-flex-align: center;
            -webkit-align-items: center;
            -webkit-box-align: center;
            align-items: center;
        }
        body.login #login {
            padding: 3% 0 5% 0;
        }
        body.login h1 a {
            background-image: url(<?php echo plugins_url( 'admin/logo.png' , dirname(__FILE__) ) ?>);
            background-size: auto;
            width: auto;
        }
        .login p#backtoblog {
            display: none;
        }
        .login.login-action-lostpassword p#nav {
            display: none;
        }
        .login.login-action-login p#nav {
            text-align: center;
        }

        body.login #nav a, body.login #backtoblog a {
            color: #4f4d4e;
            font-size: 15px;
        }
        body.login #nav a:hover, body.login #backtoblog a:hover {
            color: #222222;
        }

        body.wp-core-ui .button-primary {
            background: #ff475f;
            border-color: #ff1433 #fa0021 #fa0021;
            color: white;
            box-shadow: 0 1px 0 #fa0021;
            text-shadow: 0 -1px 1px #fa0021, 1px 0 1px #fa0021, 0 1px 1px #fa0021, -1px 0 1px #fa0021;
        }
        body.wp-core-ui .button-primary:hover, body.wp-core-ui .button-primary:focus {
            background: #ff566c;
            border-color: #fa0021;
            color: white;
            box-shadow: 0 1px 0 #fa0021;
        }
        body.wp-core-ui .button-primary:focus {
            box-shadow: inset 0 1px 0 #ff1433, 0 0 2px 1px #33b3db;
        }
        body.wp-core-ui .button-primary:active {
            background: #ff1433;
            border-color: #fa0021;
            box-shadow: inset 0 2px 0 #fa0021;
        }
        body.wp-core-ui .button-primary[disabled], body.wp-core-ui .button-primary:disabled, body.wp-core-ui .button-primary.button-primary-disabled, body.wp-core-ui .button-primary.disabled {
            color: #d1c7c8 !important;
            background: #ff1e3c !important;
            border-color: #fa0021 !important;
            text-shadow: none !important;
        }
        body.wp-core-ui .button-primary.button-hero {
            box-shadow: 0 2px 0 #fa0021 !important;
        }
        body.wp-core-ui .button-primary.button-hero:active {
            box-shadow: inset 0 3px 0 #fa0021 !important;
        }

        body input[type="checkbox"]:checked:before {
            color: #ff475f;
        }
        body input[type="checkbox"]:focus, body input[type="text"]:focus {
            border-color: #ff475f;
            -webkit-box-shadow: 0 0 2px rgba( 252, 74, 98, 0.8 );
            box-shadow: 0 0 2px rgba( 252, 74, 98, 0.8 );
        }

    </style>

    <?php
}, 100);