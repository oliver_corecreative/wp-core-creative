<?php
/**
 * Created by PhpStorm.
 * User: valleyview
 * Date: 22/01/2014
 * Time: 15:53
 */

class CC {

    static function SetUploadSize($file_size) {

        @ini_set( 'upload_max_size' , "{$file_size}M" );
        @ini_set( 'post_max_size', "{$file_size}M" );

    }

}
class CC_Shortcode {

    public function __construct($shortcode, $function = false) {

      if(is_callable($function)) {
          add_shortcode($shortcode, $function);
      } else {
          add_shortcode($shortcode, array('CC_Shortcode', 'run_shortcode'));
      }

    }

    public static function run_shortcode($atts, $content = null) {

        $args = func_get_args();

        $filename = get_template_directory().'/templates/shortcodes/' . $args[2] . '.php';

        if(file_exists($filename)){

            ob_start();
            include $filename;
            $html = ob_get_contents();
            ob_end_clean();

            return $html;

        }

        return false;

    }

}
class CC_AJAX {

    public function __construct($action, $function) {

        add_action( 'wp_ajax_nopriv_' . $action, $function );
        add_action( 'wp_ajax_' . $action, $function );

    }

}
