jQuery(document).ready(function($) {

    $(document).on("submit", ".cc_form", function(e) {

        e.preventDefault();

        var $this = $(e.target);

        var _form = {};
        _form.url = $this.attr('action');
        _form.method = $this.attr('method');
        _form.data = $this.serialize();

        $.ajax(_form.url, {
            data: _form.data,
            type: _form.method,
            dataType: "json",
            success: function(data) {

                var $alert = '';

                //clear pervious data
                $this.find('.alert, p.error, .alert-wrapper').remove();
                $this.find('.error').removeClass('error');

                if(data.status == "error") {

                    $alert = $("<div />").addClass('col-xs-12').addClass('col-12').addClass('alert-wrapper').html("<div class='alert alert-danger'><p>"+ data.messages.error +"</p></div>");
                    $this.prepend($alert);

                    for (var key in data.errors) {
                        if (data.errors.hasOwnProperty(key)) {
                            var val = data.errors[key];

                            var $element = $("[name='"+key+"'], [name='"+key+"[]']", $this);
                            $element.addClass('error');

                            var $wrapper = $element.parents('div').eq(0);
                            $wrapper.append("<p class='error'>"+val+"</p>");

                        }
                    }
                    $this.trigger('errors');

                } else if(data.status == 'force_submit') {

                    $alert = $("<div />").addClass('col-xs-12').addClass('col-12').addClass('alert-wrapper').html("<div class='alert alert-warning'><p>"+ data.messages.force_submit +"</p></div>");
                    $this.prepend($alert);

                    $this.trigger('submit.real');

                } else {

                    $alert = $("<div />").addClass('col-xs-12').addClass('col-12').addClass('alert-wrapper').html("<div class='alert alert-success'><p>"+ data.messages.success +"</p></div>");
                    $this.prepend($alert);
                    $this.get(0).reset();

                    if (data.redirect) {
                        setTimeout(function() {
                            window.location = data.redirect;
                        }, 500);
                    } else if($this.parents('.modal')) {
                        setTimeout(function() {
                            $this.parents('.modal').modal('hide');
                        }, 2000);
                    }

                }
                $this.trigger('updated', [{ status: data.status }]);


            }
        });

    });

});