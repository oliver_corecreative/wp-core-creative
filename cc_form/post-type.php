<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 02/10/2014
 * Time: 14:50
 */

function cc_add_post_types() {

    // custom post type we'll use to keep copies of the feedback items
    register_post_type( 'feedback', array(
        'labels'            => array(
            'name'               => __( 'Feedback', 'jetpack' ),
            'singular_name'      => __( 'Feedback', 'jetpack' ),
            'search_items'       => __( 'Search Feedback', 'jetpack' ),
            'not_found'          => __( 'No feedback found', 'jetpack' ),
            'not_found_in_trash' => __( 'No feedback found', 'jetpack' )
        ),
        'menu_icon'         => 'dashicons-megaphone',
        'show_ui'           => TRUE,
        'show_in_admin_bar' => FALSE,
        'public'            => FALSE,
        'rewrite'           => FALSE,
        'query_var'         => FALSE,
        'capability_type'   => 'page',
        'taxonomies' => array('cc_form_name')
    ) );


    $labels = array(
        'name' => 'Form',
        'singular_name' => 'Form Name',
        'menu_name' => 'Form'
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => false,
        'show_ui'                    => false,
        'show_in_menu'               => false,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'show_in_quick_edit'         => false
    );

    register_taxonomy( 'cc_form_name', array( 'feedback' ), $args );

}
add_action('init', 'cc_add_post_types', 100);


add_filter( 'user_can_richedit', function( $default ) {
    global $post;
    if ( 'feedback' == get_post_type( $post ) )
        return false;
    return $default;
});


function wp_get_attachment_by_post_name( $post_name ) {
    global $wpdb;

    $post = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE post_title = %s AND post_type='attachment'", $post_name ));

    if ( $post )
        return $post;
    else
        return false;
}

function force_download($file) {
    $url_safe_file = urlencode($file);
    return admin_url('admin-ajax.php') . '?action=cc_download_file&file=' . $url_safe_file;
}

add_action( 'wp_ajax_nopriv_cc_download_file', 'cc_form_force_download');
add_action( 'wp_ajax_cc_download_file', 'cc_form_force_download');

function cc_form_force_download() {

    $file_url = $_REQUEST['file'];
    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
    readfile($file_url); // do the double-download-dance (dirty but worky)

    exit;

}

function feedback_form_name_filter ($post_type) {
    global $wp_query;
    if ($post_type == 'feedback') {

        $current_type = isset( $wp_query->query_vars['cc_form_name'] ) ? $wp_query->query_vars['cc_form_name'] : '';
        $args = array(
            'pad_counts'         => 1,
            'show_count'         => 1,
            'hierarchical'       => 1,
            'hide_empty'         => 1,
            'show_uncategorized' => 0,
            'orderby'            => 'name',
            'selected'           => $current_type,
            'menu_order'         => false,
        );

        if ( 'order' === $args['orderby'] ) {
            $args['menu_order'] = 'asc';
            $args['orderby']    = 'name';
        }

        $terms = get_terms( 'cc_form_name', apply_filters( 'wc_product_dropdown_categories_get_terms_args', $args ) );

        if ( empty( $terms ) ) {
            return;
        }

        $output  = "<select name='cc_form_name' class='dropdown_product_cat'>";
        $output .= '<option value="" ' . selected( $current_type, '', false ) . '>' . esc_html__( 'Select a form', 'woocommerce' ) . '</option>';
        
        foreach ($terms as $term) {
            $output .= '<option class="level-0" value="'.$term->slug.'" ' . selected( $current_type, $term->slug, false ) . '>' . esc_html__($term->name, 'woocommerce' ) . '&nbsp;('.$term->count.')' . '</option>';
        }
        
        if ( $args['show_uncategorized'] ) {
            $output .= '<option value="0" ' . selected( $current_type, '0', false ) . '>' . esc_html__( 'Not set', 'woocommerce' ) . '</option>';
        }
        
        $output .= "</select>";

        $output .= "<style>#date { width: 25%; }</style>";

        echo $output;

    }
}
add_action('restrict_manage_posts', __NAMESPACE__ . '\\feedback_form_name_filter');

function cc_form_date_column_status( $status, $post ) {
    if($post->post_type == 'feedback') $status = '';
    return $status;
}
add_filter('post_date_column_status', 'cc_form_date_column_status', 10, 2);

function cc_form_date_column_time( $time, $post ) {
    if($post->post_type == 'feedback') {
        return date('g:ia - jS F Y', strtotime($post->post_date));
    }
    return $time;
}
add_filter('post_date_column_time', 'cc_form_date_column_time', 10, 2);


function cc_form_set_term_or_create($post_id, $term_name, $taxonomy, $append = true) {
    if(empty($term_name)) return false;
    $term = term_exists($term_name, $taxonomy);
    if (!$term) {
        $term = wp_insert_term($term_name, $taxonomy);
    }
    if (is_wp_error($term)) {
        die("Error inserting term: " . $term->get_error_message());
    }
    wp_set_object_terms($post_id, array((int)$term['term_id']), $taxonomy, $append);
    return $term;
}
