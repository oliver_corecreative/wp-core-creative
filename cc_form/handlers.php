<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 02/10/2014
 * Time: 16:09
 */

class CC_Form {

	public $forms = array();
	public static $init;
	private $current_form;


    public function __construct() {

        $this->shortcode();
        $this->hooks();

    }


    public static function get_instance() {
        static $singleton_instance = null;
        if($singleton_instance === null) {
            $singleton_instance = new CC_Form();
        }

        return($singleton_instance);
    }



    public function add_form($id, $fields, $config) {

        $this->forms[$id] = new CC_Form_Form($id, $fields, $config);

    }

    public function get_form($id) {

        if(!isset($this->forms[$id])) throw new Exception("Form '$id' does not exist.");
        return $this->forms[$id];

    }

    public static function display_form($id, $atts = array()) {

        $cc_form = CC_Form::get_instance();

        if(!isset($cc_form->forms[$id])) throw new Exception("Form '$id' does not exist.");
        $cc_form->current_form = $id;


        ob_start();
        extract($atts);
        $template_directory = apply_filters('cc_form_directory', get_template_directory() . '/templates/forms/');
        include $template_directory . $id . '.php';
        return ob_get_clean();



    }

    private function shortcode() {

        add_shortcode('cc_form', function( $atts ) {

            if(!isset($atts['id'])) return '';

            try {
                return CC_Form::display_form($atts['id'], $atts);
            } catch (Exception $e) {
                return '<code>' . $e->getMessage() . '</code>';
            }

        });

    }

    private function hooks() {

        add_action( 'wp_ajax_nopriv_cc_form', array( $this, 'handle_submission'));
        add_action( 'wp_ajax_cc_form', array( $this, 'handle_submission'));

    }

    public static function get_vars() {

        $cc_form = CC_Form::get_instance();

        $vars = new stdClass;
        $vars->fields = '<input type="hidden" name="action" value="cc_form" />
		<input type="hidden" name="id" value="' . $cc_form->current_form . '" />
		<input type="text" name="antispam" value="" style="visibility: hidden;opacity: 0;height: 1px;width: 1px;padding: 0;position: absolute;top: -9999px;left: -9999px;" />';
        $vars->url = admin_url('admin-ajax.php');

        return $vars;

    }

    public function handle_submission() {

        $id = $_REQUEST['id'];
        $form = CC_Form::get_instance()->get_form($id);
        $this->current_form = $id;

        $post_data = array();
        $errors = array();
        $status = 'success';

        foreach($form->fields as $key=>$field) {

            if(isset($_REQUEST[$key]))
                $field['value'] = $_REQUEST[$key];
            else
                $field['value'] = 0;

            // field type check for files
            if(isset($field['type']) && $field['type'] == 'file' && !cc_is_ajax() && ( (!is_array($_FILES[$key]['tmp_name']) && !empty($_FILES[$key]['tmp_name'])) || (is_array($_FILES[$key]['tmp_name']) && !empty($_FILES[$key]['tmp_name'][0])) )) {
                $field['value'] = array();

                $wp_upload_dir = wp_upload_dir();
                $upload_path = $wp_upload_dir['basedir'] . '/cc_form/';
                $upload_url = $wp_upload_dir['baseurl'] . '/cc_form/';

                if(!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                }

                $files = array();

                if(is_array($_FILES[$key]['tmp_name'])) { // multiple files
                    foreach($_FILES[$key]['tmp_name'] as $i => $tmp_name) {
                        $files[$i] = array(
                            'name' => $_FILES[$key]['name'][$i],
                            'type' => $_FILES[$key]['type'][$i],
                            'tmp_name' => $_FILES[$key]['tmp_name'][$i],
                            'error' => $_FILES[$key]['error'][$i],
                            'size' => $_FILES[$key]['size'][$i]
                        );
                    }
                } else { // just one file
                    $files[] = array(
                        'name' => $_FILES[$key]['name'],
                        'type' => $_FILES[$key]['type'],
                        'tmp_name' => $_FILES[$key]['tmp_name'],
                        'error' => $_FILES[$key]['error'],
                        'size' => $_FILES[$key]['size']
                    );
                }

                foreach($files as $file) {
                    $filename = wp_unique_filename( $upload_path, $file['name'] );
                    move_uploaded_file($file['tmp_name'], $upload_path . $filename);
                    $field['value'][] = $upload_url . $filename;
                }
            } elseif(isset($field['type']) && $field['type'] == 'file') {
                $field['value'] = "No files submitted.";
            }

            $validation = $this->validate($field);

            if($validation['status'] != 'success') {
                $status = 'error';
                $errors[$key] = $validation['message'];
            }

            if(is_array($field['value'])) {
                $field['value'] = array_filter($field['value']);
                $i = 0; foreach($field['value'] as &$val) {
                    $val = stripslashes($validation['value'][$i]);
                    $i++;
                }
                $field['value'] = nl2br(trim(implode(" \n", $field['value'])));
            } else {
                $field['value'] = stripslashes($validation['value']);
            }

            $post_data[$key] = $field;
        }

        if($this->contains_files() && cc_is_ajax() && $status == 'success') {
            die(json_encode(array(
                'status' => 'force_submit',
                'errors' => $errors,
                'messages' => array(
                    'error' => $form->config['error_message'],
                    'success' => $form->config['success_message'],
                    'force_submit' => 'Please wait whilst your files are uploaded...',
                )
            )));
        }

        $output = array(
            'status' => $status,
            'errors' => $errors,
            'messages' => array(
                'error' => $form->config['error_message'],
                'success' => $form->config['success_message']
            )
        );

        $post_data = apply_filters('cc_form_' . $form->id . '_post_data', $post_data, $form);
        if($status == 'success') {

            unset($post_data['antispam']);
            unset($post_data['g-recaptcha-response']);
            if($form->config['add_post']) {
                $this->add_post( $post_data, $form->id );
            }

            $send_email = true; // silently ignore that we didn't send an email and just think that everything went well!
            if($form->config['send_email']) {

                if(is_callable($form->config['send_email'])) {
                    $email_address = call_user_func_array($form->config['send_email'], array($post_data));
                    $form->config['send_email'] = $email_address;
                }

                $data = array( // this is used in the template
                    'form' => $form->id,
                    'fields' => $post_data,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'date' => date('r')
                );

                $email_template = "email-template";
                if($form->config['email_template']) {
                    $email_template = $form->config['email_template'];
                }

                ob_start();
                $template_directory = apply_filters('cc_form_directory', get_template_directory() . '/templates/forms/');
                include $template_directory.$email_template.'.php';
                $email_body = ob_get_clean();

                if(!is_array($form->config['send_email'])) {
                    $form->config['send_email'] = array( $form->config['send_email'] );
                }

                foreach($form->config['send_email'] as $email_address) {
                    $send_email = wp_mail($email_address, apply_filters('cc_form_' . $form->id . '_subject', 'Website Submission : ' . ucwords(str_replace('-', ' ', $form->id))), $email_body, array(
                        "From: " . get_bloginfo('name') . " <no-reply@" . $_SERVER['HTTP_HOST'] . ">",
                        "Reply-To: " . (!empty($post_data['email']['value']) ? $post_data['email']['value'] : "sales@" . $_SERVER['HTTP_HOST']),
                        "Content-type: text/html"
                    ));
                }

            }

            $callback_status = call_user_func($form->config['callback'], $post_data);

            if(!$send_email || $callback_status === FALSE || (is_array($callback_status) && isset($callback_status['status']))) {

                if(is_array($callback_status) && isset($callback_status['status'])) {
                    $output['status'] = $callback_status['status'];
                } else {
                    $output['status'] = 'error';
                }

                if(is_array($callback_status) && isset($callback_status['message'])) {
                    $output['messages']['error'] = $callback_status['message'];
                } else {
                    $output['messages']['error'] = 'There was an error with your submission. Please try again later or email direct to <a href="mailto:' . get_field('email-address', 'option') . '">' . get_field('email-address', 'option') . '</a>';
                }
            }

        }

        if(!empty($form->config['redirect'])) {
            if(is_string($form->config['redirect']))
                $output['redirect'] = $form->config['redirect'];
            elseif(is_callable($form->config['redirect'])) {
                $output['redirect'] = call_user_func($form->config['redirect'], $post_data);
            } else {
                throw new Exception('Form redirect is in an incorrect format - accepts `str` and `callable`');
            }
        }


        if(cc_is_ajax()) {
            die(json_encode($output));
        } else {
            $url = add_query_arg(urlencode_deep(array_filter($output)), $_SERVER['HTTP_REFERER']) . '#form';
            wp_redirect($url);
            exit;
        }


    }

    var $set_rules = array(
        'required',
        'email',
        'notrequired'
    );

    private function validate($field_data) {

        if($field_data['required'])
            $field_data['rules'] = trim('required|' . $field_data['rules'], '|');

        $rules = explode('|', $field_data['rules']);

        $item_failed = true;

        if(is_array($field_data['value'])) {
            foreach($field_data['value'] as &$item) {
                $item = trim($this->sanitize_input($item));
            }
            $field_data['value'] = array_filter($field_data['value']);
        } else {
            $field_data['value'] = trim($this->sanitize_input($field_data['value']));
        }

        if(!empty($rules)) {
            foreach ( $rules as $rule ) {

                if($field_data['required'] == 'conditional' && @$_REQUEST[$field_data['condition_key']] != $field_data['condition_value']) continue; // If the validation is conditional and the field doesn't match then it doesn't need validating

                $data = $this->run_validation_rule( $field_data['value'], $rule );

                if($data['status'] == 'error') {
                    $item_failed = true;
                    return array(
                        'status' => 'error',
                        'message' => sprintf($data['message'], $field_data['label']),
                        'value' => $field_data['value']
                    );
                }

            }
        }


        return array(
            'status' => 'success',
            'message' => '',
            'value' => $field_data['value']
        );


    }

    private function run_validation_rule($value, $rule) {

        $param = FALSE;
        if ( preg_match('/(.*?)\[(.*)\]/', $rule, $match))
        {
            $rule = $match[1];
            $param = $match[2];
        }

        if($rule == 'required') {

            $message = '\'%s\' is required.';
            if(is_array($value)) {
                $message = '\'%s\' are required.';
            }

            return empty($value) ? array(
                'status' => 'error',
                'message' => $message
            ) : true;

        } else if($rule == 'notrequired') {

            $message = '\'%s\' is required to not be filled.';
            if(is_array($value)) {
                $message = '\'%s\' are required to not be filled.';
            }

            return !empty($value) ? array(
                'status' => 'error',
                'message' => $message
            ) : true;

        } else if ($rule == 'email') {

            if(is_array($value)) {
                foreach($value as $val) {
                    if(!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                        return array(
                            'status' => 'error',
                            'message' => '\'%s\' must be valid email addresses.'
                        );
                    }
                }
                return true;
            }
            return !filter_var($value, FILTER_VALIDATE_EMAIL) ? array(
                'status' => 'error',
                'message' => '\'%s\' must be a valid email address.'
            ) : true;

        } else if ($rule == 'length') {

            if(!is_array($value)) {
                $value = array($value);
            }

            foreach($value as $val) {
                if(strlen(trim($val)) != $param) {
                    return array(
                        'status' => 'error',
                        'message' => '%s must be '.$param.' characters long.'
                    );
                }
            }
            return true;

        } else if ($rule == 'recaptcha' && defined('RECAPTCHA_SECRET')) {

            $recaptcha_response = wp_remote_post("https://www.google.com/recaptcha/api/siteverify", array(
                'body' => array(
                    'secret' => RECAPTCHA_SECRET,
                    'response' => $value,
                    'remoteip' => $_SERVER['REMOTE_ADDR']
                )
            ));


            $recaptcha_data = json_decode($recaptcha_response['body']);

            if($recaptcha_data->success !== true) {
                return array(
                    'status' => 'error',
                    'message' => 'You must complete the verification field.'
                );
            } else {
                return $recaptcha_data->success;
            }

        } else if(is_callable($rule)) {

            if(is_array($value)) {
                foreach($value as $val) {
                    $status = call_user_func($rule, $val);

                    if(!$status) {
                        return array(
                            'status' => 'error',
                            'message' => '%s is not in the correct format.'
                        );
                    }
                }
                return true;
            }
            $status = call_user_func($rule, $value);
            return !$status ? array(
                'status' => 'error',
                'message' => '%s is not in the correct format.'
            ) : true;

        }

        return true;

    }

    private function sanitize_input($input) {
        $input = trim(wp_strip_all_tags($input));
        return $input;
    }


    public function add_post($data, $form_id) {

        $feedback_time = current_time('mysql');
        $feedback_status = 'publish';
        $feedback_title  = $data['name']['value'] . " - {$feedback_time} (".$form_id.")";

        $feedback_title  = $data['name']['value'];
        if (isset($data['email']) && !empty($data['email']['value'])) {
            $feedback_title .= ' - ' . $data['email']['value'];
        }

        $content = '';
        foreach($data as $field) {
            $content .= $field['label'] . ': ' . $field['value'] . "\n";
        }
        $content .= "\n";
        $content .= "Form submitted on " . date('r') . " with an IP address of " . $_SERVER['REMOTE_ADDR'];
        $content .= "\n\n\n";
        $content .= "###  NOTES  ###\n";


        add_filter( 'wp_insert_post_data', array( $this, 'insert_feedback_filter' ), 10, 2 );

        $post_id = wp_insert_post( array(
            'post_date'    => addslashes( $feedback_time ),
            'post_type'    => 'feedback',
            'post_status'  => addslashes( $feedback_status ),
            'post_title'   => addslashes( wp_kses( $feedback_title, array() ) ),
            'post_content' => addslashes( wp_kses( $content, array(
                'table' => array(),
                'thead' => array(),
                'tbody' => array(),
                'tr' => array(),
                'th' => array(),
                'td' => array()
            ))), // so that search will pick up this data
            'post_name'    => md5( $feedback_title ),
        ) );

        // once insert has finished we don't need this filter any more
        remove_filter( 'wp_insert_post_data', array( $this, 'insert_feedback_filter' ), 10, 2 );

        cc_form_set_term_or_create($post_id, ucwords(trim(str_replace(array("-", "_"), array(" ", " "), $form_id))), 'cc_form_name', false);

        return $post_id;

    }
    function insert_feedback_filter( $data, $postarr ) {
        if ( $data['post_type'] == 'feedback' && $postarr['post_type'] == 'feedback' ) {
            $data['post_author'] = 0;
        }

        return $data;
    }

    function contains_files() {

        $current_form = CC_Form::get_instance()->get_form($this->current_form);
        foreach($current_form->fields as $key => $field) {
            if(isset($field['type']) && $field['type'] == 'file') return true;
        }
        return false;
    }

}
