<?php

require 'post-type.php';
require 'handlers.php';


add_action( 'wp_enqueue_scripts', function() {

	wp_enqueue_script( 'cc_form_js', plugins_url('inc/cc_form.js', __FILE__), array('jquery'), null, true );

}, 101 );

class CC_Form_Form {

	public $id = array();
	public $fields = array();
	public $config = array();

	public function __construct($id, $fields, $config = array()) {


		// validate and set id
		if(!is_string($id)) throw new Exception("CC_Form::__construct(); \$id parameter must be a string. " . gettype($id) . ' given.');
		$this->id = $id;

		// validate and set fields
		if(!is_array($fields)) throw new Exception("CC_Form::__construct(); \$fields parameter must be an array. " . gettype($fields) . ' given.');

		$field_defaults = array(
			'label' => null,
			'email' => true,
			'required' => false,
			'rules' => ''
		);

		$fields['antispam'] = array(
			'label' => 'Anti-Spam field',
			'email' => false,
			'required' => false,
			'rules' => 'notrequired'
		);
		foreach($fields as $name=>$field) {

			if(is_string($field)) {
				$field = array(
					'label' => $field
				);
			}

			$this->fields[$name] = array_merge($field_defaults, $field);

		}


		// validate and set config
		if(!is_array($config)) throw new Exception("CC_Form::__construct(); \$config parameter must be an array. " . gettype($config) . ' given.');

		$config_defaults = array(
			'send_email' => get_option('admin_email'),
			'send_email_customer' => false,
			'error_message' => 'There were errors with your submission. Please correct the errors below and try again.',
			'success_message' => 'Thank you for your enquiry. A member of our team will contact you shortly.',
			'redirect' => function() { return false; },
			'callback' => function() { return; },
			'add_post' => true
		);

		$this->config = array_merge($config_defaults, $config);

		if($this->config['send_email'] === true)
			$this->config['send_email'] = get_option('admin_email');

	}

}
