<?php
/*
Plugin Name: Core Creative
Version: 1
Plugin URI: http://corecreative.co.uk/
Author: Oliver Sadler
Author URI: http://oliversadler.co.uk/
Description: Set of Core Creative utilities
*/
include 'admin/admin.php';
include 'utilities.php';
include 'cc.class.php';

include 'cc_form/cc_form.php';
include 'cc_modal/cc_modal.php';
